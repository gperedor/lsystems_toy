# L-Systems Toy

![Quadratic Koch Curve](/screenshots/quadratic_koch_curve.png)

A work-in-progress Processing sketch for playing with
L-Systems.

It currently features the required basic L-Systems
generator and interpreter accompanied with a small
turtle-graphics system to actually draw the figures,
frame by frame.

## Usage

Download the repository (e.g.: in the Downloads section to the left) and
unzip it. Rename the resulting folder to "lsystems\_toy" and open any file
with Processing.

## References:

- Prusinkiewicz P., Lindenmayer A (2004). [The Algorithmic Beauty of Plants](http://algorithmicbotany.org/papers/abop/abop.pdf).
