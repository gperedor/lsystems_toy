import java.util.Stack;

/**
 * A custom-made, trivial turtle-graphics system.
 * See: https://en.wikipedia.org/wiki/Turtle_graphics
 *
 * It keeps an internal state of position, angle and stroke
 * length; it uses a stack to be able to trace back to a previous
 * position.
 */
 
class Turtle {
  class State {
    int x;
    int y;
    float angle;
    int length;
  }
  
  /**
   * The current turtle State. It's supposed to be at the top
   * of the states stack as well.
   */
  State current;

  /**
   * A stack of turtle States.
   */
  Stack<State> states;
  
  Turtle(int x, int y, float angle, int length) {
    current = new State();
    current.x = x;
    current.y = y;
    current.angle = angle;
    current.length = length;
    
    states = new Stack<State>();
    states.push(current);
  }
  
  /**
   * Draws a segment in the direction given by the turtle's angle
   * and stroke length.
   */
  void forward() {
    int end_x = current.x + (int)(current.length*cos(current.angle));
    int end_y = current.y + (int)(current.length*sin(current.angle));
    line(current.x, current.y, end_x, end_y);
    
    current.x = end_x;
    current.y = end_y;
  }
  
  /**
   * Changes the direction the turtle is facing, relative to its current
   * orientation, adding the given angle
   */
  void rotate(float angle) {
    current.angle += angle;
  }
  
  /**
   * Saves the current turtle state, to be restored with restore()
   */
  void record() {
    State s = new State();
    s.x = current.x;
    s.y = current.y;
    s.angle = current.angle;
    s.length = current.length;
    
    current = s;
    states.push(s);
  }
  
  /**
   * Restores the previous saved turtle state.
   */
  void restore() {
    states.pop();
    current = states.peek();
  }
}