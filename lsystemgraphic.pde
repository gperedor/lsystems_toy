/**
 * An L-Systems drawing class.
 * Its parameterized with the L-System proper and uses
 * a custom-made Turtle graphics system to perform stepwise
 * animations.
 *
 * Its core logic uses a simple switch-based interpreter
 * to control the turtle cursor.
 */
class LSystemGraphic {
  Turtle turtle;
  int step;
  color strokeColor;
  // TODO: introduce stack
  int stepScaleFactor;

  LSystem system;
  
  LSystemGraphic(int x, int y, color strokeColor, int baseLength, int stepScaleFactor, LSystem system) {
    this.turtle = new Turtle(x, y, 0, baseLength);
    
    this.strokeColor = strokeColor;
    this.stepScaleFactor = stepScaleFactor;
    
    this.system = system;
    this.step = 0;
  }
  
  /**
   * Draws a shape based on the instructions given by the L-System
   */
  void draw() {
    String spec = system.spec;
    pushStyle();
    stroke(strokeColor);
    
    if (step == 0) {
      turtle.record();
    }
    
    if (step < spec.length()) {
      switch (spec.charAt(step)) {
        case 'F':
          turtle.forward();
          break;
        case '-':
          turtle.rotate(HALF_PI);
          break;
        case '+':
          turtle.rotate(-HALF_PI);
          break;
        default:
          break;
      }
      step++;
    } else {
      step = 0;
      turtle.restore(); 
    }
    popStyle();
  }
}