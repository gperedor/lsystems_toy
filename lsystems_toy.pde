import java.util.LinkedList;

/*
 * Draws L-System thingies, currently Koch curves
 * http://algorithmicbotany.org/papers/abop/abop-ch1.pdf
 */

class KochCurve implements Generator {
  String step(String val) {
    return val.replace("F", "F+F-F-F+F"); 
  }
}

class QuadraticKochCurve implements Generator {
  String step(String val) {
    return val.replace("F", "F+F-F-FF+F+F-F"); 
  }
}

LinkedList<LSystemGraphic> systems = null;

void setup() {
  size(430, 405);
  background(#48647F);
  systems = new LinkedList<LSystemGraphic>();
  
  LSystem k = new LSystem("F", 3, new QuadraticKochCurve());

  LSystemGraphic lsg = new LSystemGraphic(50, 200, color(#DCEEFF), 5, 1, k);
  systems.add(lsg);
}

void draw() {
  for (LSystemGraphic lsg : systems) {
    lsg.draw(); 
  }
}