interface Generator {
  String step(String val);
}

/**
 * A parametric L-System class.
 * It receives a Generator instance that contains the production rules.
 */
class LSystem {
  public final String spec;
  final Generator g;
  
  String generate(String initial, int steps) {
    String s = initial;
    
    for (int i = 0; i < steps; i++) {
      s = g.step(s);
    }
    return s;
  }

  LSystem(String initial, int steps, Generator g) {
    this.g = g;
    this.spec = generate(initial, steps);
  } 
}